package rs.ava.task.test;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import rs.ava.task.DTO.*;
import rs.ava.task.services.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LocationServiceTest {
	
	@Autowired
	private LocationService locationService;
	@Autowired
	private GoogleMapsService mapsService;
	
	
	@Test
	public void test_generateURI(){
		
		String expected = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=44.803677,20.479963&rankby=distance&types=cafe&key=AIzaSyA5adWH19PtvgINqicmg05LelDVqcuW-xg";		
		
		CoordinatesDTO location = new CoordinatesDTO(44.803677, 20.479963);

		String actual = mapsService.generateUrlWithRankBySearch(location);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void test_meetAtCafe() throws Exception{
		
		CoordinatesDTO location = new CoordinatesDTO(44.803677, 20.479963);
		CafeDTO actual = (CafeDTO) locationService.meetAtCafe(location);
		
		System.out.println(actual.getCafeName());
		System.out.println(actual.getLatitude());
		System.out.println(actual.getLongitude());
		
		//assertEquals(expected, actual);
		
	}
}
