$(document).ready(function() {
	var self = this;
	
	var userTemplateSel= "#user-template";
	var warningMessageTemplateSel= "#warning-message-template";
	
	//validate longitude and latitude
	self.isCoordinatesValid = function(coordinates){
		
		var invalidCoordinates = coordinates.filter(function(index, element){
				var lat = $(element).find('.latitude').val();
				var lon = $(element).find('.longitude').val();
				return !lat || !lon || isNaN(lat) || isNaN(lon);
			});
		if(invalidCoordinates.length > 0){
			self.warningMessage("Please, fill all latitude and longitude.");
			return false;
		}
		return true;
	}
	
	self.prepareCoordinateForAjax = function(coordinates){
		return $.map(coordinates, function(element) {
			return {
				latitude : $(element).find('.latitude').val(),
				longitude : $(element).find('.longitude').val(),
			}
		});
	}
	
	self.warningMessage = function(m){
		//show warning message
		var theTemplateScript = $(warningMessageTemplateSel).html();
		var theTemplate = Handlebars.compile(theTemplateScript);
		var theCompiledHtml = theTemplate({message: m});
		$('#message').html(theCompiledHtml);
	}
	
	$('#getLocation').click(function(){
		$('#message').html("");
		var coordinates = $('.user');
		if(!self.isCoordinatesValid(coordinates)) return;
		
		var data = self.prepareCoordinateForAjax(coordinates);

		$.ajax({
			type : "POST",
			url : "/meetAt",
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			data : JSON.stringify(data)
		}).done(function(data){
			$('#meetAt').html(data.latitude + ', ' + data.longitude);
			$.ajax({
				type : "POST",
				url : "/meetAtCafe",
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				data : JSON.stringify(data),
			}).done(function(data){
				$('#closestCaffee').html(
						data.cafeName + "(" + data.latitude + ","
								+ data.longitude + ")");
			}).fail(function(){
				$('#closestCaffee').html("");
				self.warningMessage("We can't find cafe for you.");
			});
		}).fail(function(){
			$('#meetAt').html("");
			self.warningMessage("We can't find cafe for you.");
		});
	});
	
	$('#addPerson').click(function(){
		var personName = $('#newPersonName').val();
		
		var theTemplateScript = $(userTemplateSel).html();
		var theTemplate = Handlebars.compile(theTemplateScript);
		var theCompiledHtml = theTemplate({name: personName});

		$('.person-content').append(theCompiledHtml);
		
		$('#newPersonName').val("");
		$('#newPersonModal').modal('hide');
		
	});	

	$('#showAddPersonModal').click(function() {
		$('#message').html("");
		$('#newPersonModal').modal();
	});
});