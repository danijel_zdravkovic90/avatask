package rs.ava.task.controllers;

import java.util.List;
import java.util.Locale;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import rs.ava.task.DTO.CoordinatesDTO;
import rs.ava.task.services.LocationService;

@Controller
public class IndexController {

	@RequestMapping("/")
	String index(Locale locale, Model model){
		return "index";
	}
}
