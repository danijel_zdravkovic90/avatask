package rs.ava.task.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.ava.task.DTO.CoordinatesDTO;
import rs.ava.task.services.LocationService;

@RestController
public class MeetingController {
	
	@Autowired
	private LocationService locationService;
	
	@RequestMapping(value="/meetAt", method=RequestMethod.POST,  produces = "application/json")
	CoordinatesDTO meetAt(@RequestBody CoordinatesDTO[] locations) throws Exception{
		return locationService.meetAt(locations);
	}
	
	@RequestMapping(value="/meetAtCafe", method=RequestMethod.POST,  produces = "application/json")
	CoordinatesDTO meetAtCafe(@RequestBody CoordinatesDTO location) throws Exception{
		return locationService.meetAtCafe(location);
	}
}
