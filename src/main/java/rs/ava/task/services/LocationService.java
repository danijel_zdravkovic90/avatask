package rs.ava.task.services;

import java.io.*;
import java.net.*;
import java.util.*;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ava.task.SpringBootWebApplication;
import rs.ava.task.DTO.*;

@Service
public class LocationService {

	@Autowired
	private GoogleMapsService mapsService;
	
	@Autowired
	private CoordinatesService coordinatesService;

	private static final Logger log = LoggerFactory.getLogger(SpringBootWebApplication.class);

	public CoordinatesDTO meetAt(CoordinatesDTO[] locations) throws Exception {
		return coordinatesService.calculateMidpoint(locations);
	}

	// get nearest cafe from google maps api
	public CoordinatesDTO meetAtCafe(CoordinatesDTO location) throws Exception {
		URLConnection urlConnection = null;
		URL url = null;
		InputStreamReader inputStreamReader = null;
		CafeDTO retCafeDTO = null;
		CafeDTO cafeDTO = null;
		ArrayList<CafeDTO> cafeDTOs = new ArrayList<CafeDTO>();
		String nextToken = null;
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode res = null;

		try {

			url = new URL(mapsService.generateUrlWithRankBySearch(location));
			urlConnection = url.openConnection();
			inputStreamReader = new InputStreamReader(urlConnection.getInputStream());
			res = objectMapper.readValue(inputStreamReader, JsonNode.class);

			retCafeDTO = mapsService.getCafeInfo(res, 0);

			inputStreamReader.close();
			((HttpURLConnection) urlConnection).disconnect();

			if (retCafeDTO != null)
				return retCafeDTO;

			url = new URL(mapsService.generateUrlWithRadiusSearch(location, null));

			while (true) {
				urlConnection = url.openConnection();
				inputStreamReader = new InputStreamReader(urlConnection.getInputStream());
				res = objectMapper.readValue(inputStreamReader, JsonNode.class);

				int i = 0;
				while (true) {
					cafeDTO = mapsService.getCafeInfo(res, i++);
					if (cafeDTO == null)
						break;
					cafeDTOs.add(cafeDTO);
				}

				nextToken = mapsService.getNextToken(res);

				inputStreamReader.close();
				((HttpURLConnection) urlConnection).disconnect();

				if (nextToken == null)
					break;

				url = new URL(mapsService.generateUrlWithRadiusSearch(location, nextToken));
			}
			
			if(cafeDTOs.size() == 0) throw new Exception("No result");
			
			for (int k = 0; k < cafeDTOs.size(); k++)
				cafeDTOs.get(k).setDistance(coordinatesService.calculateDistance(location, cafeDTOs.get(k)));
			Collections.sort(cafeDTOs);
			return cafeDTOs.get(0);
		} catch (Exception e) {

			if (inputStreamReader != null)
				inputStreamReader.close();
			log.error("Error:", e);
			throw e;
		}
	}
}