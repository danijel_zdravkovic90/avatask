package rs.ava.task.services;

import org.springframework.stereotype.Service;

import rs.ava.task.DTO.CoordinatesDTO;

@Service
public class CoordinatesService {
	
	private static final double R = 6371;
	
	// calculate midpoint by transform to Cartesian coordinates, add each of
	// coordinates, and transform them back to geographic coordinates
	public CoordinatesDTO calculateMidpoint(CoordinatesDTO[] locations) throws Exception{

		CoordinatesDTO result = new CoordinatesDTO();

		double x = 0, y = 0, z = 0;
		double lat = 0, lon = 0;

		for (int i = 0; i < locations.length; i++) {

			validateCoordinates(locations[i]);

			// convert to radians
			lat = Math.toRadians(locations[i].getLatitude());
			lon = Math.toRadians(locations[i].getLongitude());

			// convert to Cartesian coordinates
			x += Math.cos(lat) * Math.cos(lon);
			y += Math.cos(lat) * Math.sin(lon);
			z += Math.sin(lat);
		}

		x = x / (double) locations.length;
		y = y / (double) locations.length;
		z = z / (double) locations.length;

		// concert to geographic coordinates
		result.setLongitude(roundToSixDecimals(Math.toDegrees(Math.atan2(y, x))));
		result.setLatitude(roundToSixDecimals(Math.toDegrees(Math.atan2(z, Math.sqrt(x * x + y * y)))));

		return result;

	}

	//calculate distance between two coordinates
	public double calculateDistance(CoordinatesDTO c1, CoordinatesDTO c2) {
		double dLat = Math.toRadians(c2.getLatitude() - c1.getLatitude());
		double dLon = Math.toRadians(c2.getLongitude() - c1.getLongitude());
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(c1.getLatitude()))
				* Math.cos(Math.toRadians(c2.getLatitude())) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return R * c;

	}
	
	// validate geographic coordinates
	private void validateCoordinates(CoordinatesDTO location) throws Exception {
		if (location.getLatitude() > 90.0 || location.getLatitude() < -90.0 || location.getLongitude() > 180
				|| location.getLongitude() < -180)
			throw new Exception("Bad coordinates");
	}
	
	// round double number to six decimals
	private double roundToSixDecimals(double d) {
		return Math.round(d * 1000000.0) / 1000000.0;
	}

}
