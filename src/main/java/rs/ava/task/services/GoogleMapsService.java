package rs.ava.task.services;

import java.io.IOException;
import java.io.InputStreamReader;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import rs.ava.task.DTO.*;

@Service
public class GoogleMapsService {

	private static final String PLACES_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";// "https://maps.googleapis.com/maps/api/place/search/json?rankby=distance&";
	private static final String API_KEY = "AIzaSyA5adWH19PtvgINqicmg05LelDVqcuW-xg";
	private static final String STATUS_CODE_OK = "OK";
	private static final String STATUS_CODE_ZERO_RESULTS = "ZERO_RESULTS";

	public String generateUrlWithRankBySearch(CoordinatesDTO coordinates) {
		
		UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(PLACES_SEARCH_URL)
				.queryParam("location", coordinates.getLatitude() + "," + coordinates.getLongitude())
				.queryParam("rankby", "distance").queryParam("types", "cafe").queryParam("key", API_KEY);

		return uri.build().toString();
	}

	public String generateUrlWithRadiusSearch(CoordinatesDTO coordinates, String pageToken) {
		
		UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(PLACES_SEARCH_URL)
				.queryParam("location", coordinates.getLatitude() + "," + coordinates.getLongitude())
				.queryParam("radius", "50000").queryParam("types", "cafe").queryParam("key", API_KEY);

		if (pageToken != null)
			uri.queryParam("pagetoken", pageToken);

		return uri.build().toString();
	}

	// return coordinates of nearest cafe
	// return null if status code is ZERO RESULT
	public CafeDTO getCafeInfo(JsonNode res, int resultIndex) throws Exception {

		CafeDTO cafeDTO = new CafeDTO();

		String statusCode = res.get("status").asText();

		if (statusCode.equals(STATUS_CODE_ZERO_RESULTS))
			return null;

		if (!statusCode.equals(STATUS_CODE_OK))
			throw new Exception("No result");

		JsonNode results = res.get("results");
		JsonNode result = results.get(resultIndex);
		if (result == null)
			return null;
		JsonNode locationJsonNode = result.get("geometry").get("location");

		cafeDTO.setCafeName(result.get("name").asText());
		cafeDTO.setLatitude(locationJsonNode.get("lat").asDouble());
		cafeDTO.setLongitude(locationJsonNode.get("lng").asDouble());

		return cafeDTO;
	}

	public String getNextToken(JsonNode res) throws Exception {

		JsonNode nextPageToken = res.get("next_page_token");

		if (nextPageToken == null)
			return null;
		return nextPageToken.asText();
	}
}
