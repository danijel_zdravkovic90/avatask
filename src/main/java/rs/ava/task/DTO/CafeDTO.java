package rs.ava.task.DTO;

public class CafeDTO extends CoordinatesDTO implements Comparable<CafeDTO>{
	
	private String cafeName;
	private double distance;
	
	public String getCafeName() {
		return cafeName;
	}
	public void setCafeName(String cafeName) {
		this.cafeName = cafeName;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	@Override
	public int compareTo(CafeDTO o) {
		if(o.getDistance() == distance) return 0;
		if(o.getDistance() > distance) return -1;
		return 1;
	}
	
	
}
