package rs.ava.task.DTO;

public class CoordinatesDTO {
	private double latitude;
	private double longitude;

	public CoordinatesDTO(){};
	
	public CoordinatesDTO(double latitude, double longitude){
		this.latitude = latitude;
		this.longitude = longitude;
	}	
	

	public double getLatitude() {
		return Math.round(latitude * 1000000.0)/1000000.0;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return Math.round(longitude * 1000000.0)/1000000.0;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
